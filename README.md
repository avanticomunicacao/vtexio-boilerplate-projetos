# VTEX - [Store Theme][store-theme]

Our boilerplate theme to create stores in the VTEX IO platform.

![GitHub last commit][github-last-commit] ![GitHub issues][github-issues] ![GitHub pull requests][github-pull-requests]

## Avanti - [Boilerplate + SASS][boilerplate]

<!-- Por mais que seja tentador adicionar as mesmas flags ao repositório do projeto, como ele se trata de um repo -->

### Instalando vtex

Se esse é o primeiro IO, certifique-se de instalar o toolbelt da VTEX `https://github.com/vtex/toolbelt`

```
yarn global add vtex
```

Depois rode

```
yarn global bin
```

E copie o path `https://prnt.sc/11b9207`

### Configuração de Workspace

1.  No terminal de sua escolha entre na pasta `store-theme`;
2.  Loge na conta da loja `vtex login namestoreio`;
3.  `vtex workspace list` veja todos os workspaces já criados;
4.  Crie seu WS `vtex use NOMEWORKSPACE`;
5.  E por último siga as instruções para rodar o SASS e a store;

### 🛫 Iniciando o projeto

> Lembre-se de criar as variáveis necessárias de acordo com o layout.

#### 🎨 Storefront

1. Entre na pasta do projeto:

   ```bash
   cd store-theme
   ```

2. Instale as dependências:

   ```bash
   yarn
   ```

##### Abra dois terminais e execute os seguintes comandos

> Isso facilitará o desenvolvimento, e você poderá ver as alterações em tempo real

1. No primeiro terminal, execute o seguinte comando:

   Com esse comando, você irá compilar os estilos e ficará monitorando as alterações no projeto.

   ```bash
   yarn dev:sass
   ```

2. No segundo terminal, execute o seguinte comando:

   Com esse comando, você iniciará o `link` do projeto, e ficará monitorando as alterações.

   ```bash
   vtex link
   ```

### Quando começamos IO...

..Alguns erros podem ocorrer, isto acontece por falta de dependências instaladas na store e no WS (ambas poderão ser encontradas no arquivo "manifest.json" e executando o comando "vtex list" no terminal), então certifique-se de instalar:

```
vtex install vtex.search@2.x
vtex install vtex.search-result@3.x
vtex install vtex.search-resolver@1.x
```

Pode ser necessário instalar também o aplicativo [Reviews and Ratings](https://developers.vtex.com/docs/apps/vtex.reviews-and-ratings) da VTEX, primeiro verifique se será usado na loja, do contrário, remova da store.
Se for usar, instale com os seguintes comandos:

```
vtex install vtex.reviews-and-ratings@3.x
vtex install vtex.availability-notify@1.x
```

### Existem dois aplicativos que **PRECISAM** ser instalados...

Para que o boilerplate funcione no seu novo projeto, você precisará "clonar" e instalar os apps abaixo. [Link do Repositório](https://bitbucket.org/avanticomunicacao/vtexio-avantiimplatancao-apps/src/master/).
Antes da instalação, verifique nas subpastas quais apps serão usados no projeto, se não irá ser usado, remova da pasta.

```
avanti-menu
custom-apps
```

## Fazendo um deploy

#### Ambos app e store

Faça a primeira linha somente se não estiver em uma branch origin, Ex: master, development

```
git push --set-upstream origin minha-branch
vtex release patch stable
vtex publish
vtex deploy --force
vtex install
```

A store roda o publish automático após rodar o release, então não precisa rodar essa linha

## Para fazer um deploy de minor substitua patch por minor

```
vtex release minor stable
```

#### 💳 Checkout

> Lembre-se de criar as variáveis necessárias de acordo com o layout.

1. Entre na pasta do projeto:

   ```bash
   cd checkout
   ```

2. Instale as dependências:

   ```bash
   yarn
   ```

3. Rode o projeto com o comando:

   ```bash
   yarn dev
   ```

### ⚠️ Cuidados

Estes são alguns cuidados que você deve ter ao desenvolver um projeto.

#### 🛠️ Setup

Sempre que for necessário usar o `vtex setup`, para que sejam feitas as alterações necessárias, leve em consideração o seguinte:

- O `vtex setup` deve ser executado na pasta do projeto, e não na pasta raiz.
- O `vtex setup` deve ser executado após o `vtex link`, para que somente se o problema persistir, seja necessário executar o `vtex setup`.
- Os comando `vtex setup` e `vtex setup --typings` tem objetivos diferentes, e devem ser executados de acordo com a necessidade.
  - `vtex setup` - Instala as dependências do projeto. Útil quando alguma alguma nova dependência é adicionada ao `manifest.json`.
  - `vtex setup --typings` - Instala os tipos do projeto. Útil quando as dependências do projeto foram instaladas, mas a `IDE` não reconhece os tipos.
  - Existe também o comando `vtex setup --ignore-linked`

##### 🐶 Husky

- O husk vem instalado na store e apps como default vtex, para evitar conflito com ele utiliza-se do padrão de commit do git ex:
  ```
  feature(home): banner full
  ```

### 📦 Commitando

1. Inicie atualizando o repositório local:

   ```bash
   git pull develop
   ```

2. Crie uma nova branch:

   ```bash
   git checkout -b feature/feature-name
   ```

3. Faça as alterações necessárias e adicione-as ao commit:

   ```bash
   git add .
   ```

4. Rode o Gulp para garantir a padronização do código:

   ```bash
   yarn dev:sass
   ```

5. Faça o commit das alterações:

   ```bash
   git commit -m "..."
   ```

### 🌎 Roadmap

Nossos objetivos e ideias para o futuro desse boilerplate:

- [x] React na Store
- [x] Builder de assets
- [x] Componentizar Home
- [x] Componentizar PDP
- [x] Componentizar Search
- [x] Componentizar Product summary
- [x] Componentizar Footer
- [x] Componentizar Header
- [x] Componentizar Minicart
- [x] Componentizar Institucional
- [x] Componentizar Fale Conosco com o schema pronto
- [x] Estilização de páginas institucionais
- [x] Ajustes gerais em checkout
- [x] Nomenclatura site editor
- [x] Exemplo consumindo contexto de Produto
- [x] Exemplo consumindo contexto de Busca
- [x] Organização pastas CSS
- [ ] App com schema completo
- [ ] Exemplo de página de busca customizada
- [x] Criar um Readme bom
- [x] Atualizar reviews and ratings para `3.x`

<!-- Variables -->

[store-theme]: https://github.com/vtex-apps/store-theme
[boilerplate]: https://bitbucket.org/avanticomunicacao/vtexio-boilerplate-projetos
[github-last-commit]: https://img.shields.io/github/last-commit/vtex-apps/store-theme/master?style=flat-square
[github-issues]: https://img.shields.io/github/issues/vtex-apps/store-theme?style=flat-square
[github-pull-requests]: https://img.shields.io/github/issues-pr/vtex-apps/store-theme?style=flat-square
