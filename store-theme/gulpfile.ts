import gulp from "gulp";
import dartSass from 'sass';
import path from "path";

import gulpSass from 'gulp-sass';

import { DebuggerLogger } from "./gulp/helpers/debugger-logger/debugger-logger";
import { logStatistic } from "./gulp/helpers/debugger-logger/builders";
import { ManagerSassCompiler } from "./gulp/helpers/manager-sass-compiler";

const sass = gulpSass(dartSass);

const basePath = path.resolve(__dirname, '../')
const debuggerLogger = new DebuggerLogger();

const managerSassCompiler = new ManagerSassCompiler(basePath, debuggerLogger)

gulp.task('runInitialTasks', async (done) => {
  console.log('inicio')
  await managerSassCompiler.generatePathApps()
  await managerSassCompiler.compileInitialSASSInApps(gulp, sass)
  await managerSassCompiler.compileInitialSassInStoreTheme(gulp, sass)

  Object.keys(managerSassCompiler.statisticAppFiles)
    .sort((a, b) => {
      const valueA = (managerSassCompiler.statisticAppFiles as any)?.[a]
      const valueB = (managerSassCompiler.statisticAppFiles as any)?.[b]
      return valueB - valueA
    })
    .forEach((key: any) => {
      const value = (managerSassCompiler.statisticAppFiles as any)?.[key]

      logStatistic(`${key}`, `${value} files compiled`, 'app', 50)
    })

  Object.keys(managerSassCompiler.statisticStoreFiles)
    .sort((a, b) => {
      const valueA = (managerSassCompiler.statisticStoreFiles as any)?.[a]
      const valueB = (managerSassCompiler.statisticStoreFiles as any)?.[b]
      return valueB - valueA
    })
    .forEach((key: any) => {
      const value = (managerSassCompiler.statisticStoreFiles as any)?.[key]

      logStatistic(`${key}`, `${value} files compiled`, 'storeTheme', 50)
    })
  done()
})

gulp.task('runWatchTasks', () => {
  const path = __dirname
  const patchsToWatch = [
    ...managerSassCompiler.listAppPathsToWatch,
    `${basePath}/store-theme/styles/sass/**/*.scss`
  ]

  gulp.watch(patchsToWatch, { cwd: path, }).on("change", async (file) => {
    const isAppFile = file.includes('react')

    if (isAppFile) {
      await managerSassCompiler.compileWatchSASSInApps(gulp, sass, file)
    } else {
      await managerSassCompiler.compileWatchSASSInStoreTheme(gulp, sass, file)
    }
  })
})



gulp.task("store-theme", gulp.series('runInitialTasks', 'runWatchTasks'));
