import { glob } from "glob";
import path from "path";
import { gulpDebug } from "./gulp-debug";
import { getManifest } from "./get-manifest";
import { DebuggerLogger } from "./debugger-logger/debugger-logger";
import { Gulp } from "gulp";
import concat from "gulp-concat";
import { logInitialTask, logWatchTask } from "./debugger-logger/builders";

export class ManagerSassCompiler {
  constructor(
    private readonly basePath: string,
    private readonly debuggerLogs: DebuggerLogger
  ) {
  }

  // app-exemplo
  public listNameAppsToCompile: string[] = []
  // avantiimplantacao.app-exemplo.scss
  public listAppsInSASSToCompile: string[] = []

  // [
  //   C:\\XX\\boilerplate\\app-exemplo-1\\react\\**\\*.scss'
  //   C:\\XX\\boilerplate\\app-exemplo-2\\react\\**\\*.scss'
  // ]
  public listAppPathsToWatch: string[] = []

  // [vtex.flex-layout.scss]
  public listStoreNameFiles: string[] = []
  public listStorePathFiles: string[] = []

  public statisticAppFiles = {}
  public statisticStoreFiles = {}

  public generatePathApps = async (): Promise<void> => {
    await new Promise<void>((resolve) => {
      glob(`${this.basePath}/*/manifest.json`, async (err, files) => {
        const promises = files
          .filter(file => !file.includes('store-theme'))
          .map(async file => {
            const appName = path.basename(path.dirname(file))
            const manifestData = await getManifest(file)
            const appPath = `${this.basePath}/${appName}/react/**/*.scss`

            this.listAppPathsToWatch.push(appPath)
            this.listNameAppsToCompile.push(appName)
            this.listAppsInSASSToCompile.push(manifestData.file)
          })

        await Promise.all(promises)
        resolve()
      })
    });

    await new Promise<void>((resolve) => {
      glob(`${this.basePath}/store-theme/styles/sass/**/*.scss`, async (err, files) => {

        const sumFilesByBaseName = files.reduce((acc: any, file: any) => {
          const baseName = path.basename(file);

          if (!acc[baseName]) {
            acc[baseName] = 0;
          }

          acc[baseName] += 1;

          return acc;
        }, {});

        this.statisticStoreFiles = sumFilesByBaseName

        const promises = files
          .map(async file => {
            const storeBaseName = path.basename(file)

            this.listStoreNameFiles.push(storeBaseName)
            this.listStorePathFiles.push(file)
          })


        await Promise.all(promises)
        resolve()
      })
    });


    this.listNameAppsToCompile.forEach(folderName => {
      logInitialTask(`[APP NAME TO COMPILE]`, folderName, 'app')
    })

  }


  public compileInitialSASSInApps = async (gulp: Gulp, sass: any): Promise<void> => {
    const promises = this.listNameAppsToCompile.map(async (appName: string) => {
      const pathBase = `${this.basePath}/${appName}`
      const pathAppFolderFilesSASS = `${pathBase}/react/**/*.scss`
      const pathManifest = `${pathBase}/manifest.json`
      const pathDestinyCSSCompiled = `${this.basePath}/store-theme/styles/css`

      const manifestData = await getManifest(pathManifest)

      const pathFilesInSASSToCompileToCSS = [
        "styles/sass/utils/_mixin.scss",
        "styles/sass/utils/_vars.scss",
        pathAppFolderFilesSASS
      ]


      const statisticFilesOnApp = await new Promise(resolve => {
        glob(pathAppFolderFilesSASS, function (er, files) {
          resolve(files);
        });
      })

      // @ts-ignore
      this.statisticAppFiles[appName] = (statisticFilesOnApp as string[]).length;

      return new Promise(resolve => {
        gulp.src(pathFilesInSASSToCompileToCSS)
          .pipe(concat(manifestData.file))
          .pipe(sass().on("error", sass.logError))
          .pipe(gulp.dest(pathDestinyCSSCompiled))
          .on('end', resolve)
          .pipe(gulpDebug(this.debuggerLogs, 'css', 'taskInitial', 'app'))
      })
    })

    await Promise.all(promises)
  }

  public compileInitialSassInStoreTheme = async (gulp: Gulp, sass: any): Promise<void> => {
    this.debuggerLogs.logSeparator()


    const pathDestinyCSSCompiled = `${this.basePath}/store-theme/styles/css`

    const listFileNamesUniqueValues = [...new Set(this.listStoreNameFiles)]

    const promises = listFileNamesUniqueValues.map(async (fileName: string) => {
      logInitialTask(`[SCSS FILE TO COMPILE]`, fileName, 'storeTheme')
      return new Promise(resolve => {
        gulp.src([
          "styles/sass/utils/_mixin.scss",
          "styles/sass/utils/_vars.scss",
          `${this.basePath}/store-theme/styles/sass/**/${fileName}`
        ])
          .pipe(gulpDebug(this.debuggerLogs, 'scss', 'taskInitial', 'storeTheme'))
          .pipe(concat(fileName))
          .pipe(sass().on("error", sass.logError))
          .pipe(gulp.dest(pathDestinyCSSCompiled))
          .on('end', resolve)
          .pipe(gulpDebug(this.debuggerLogs, 'css', 'taskInitial', 'storeTheme'))
      })


    });
    await Promise.all(promises)

    this.debuggerLogs.logSeparator()
  }

  public compileWatchSASSInApps = async (gulp: Gulp, sass: any, file: string): Promise<void> => {
    const paths = file.split(/[\\\/]/).filter(item => item !== '..')
    logWatchTask(`[SCSS FILE CHANGED]`, paths.join('/'), 'app')
    const appName = paths[0]
    const pathBase = `${this.basePath}/${appName}`
    const pathManifest = `${pathBase}/manifest.json`
    const pathDestinyCSSCompiled = `${this.basePath}/store-theme/styles/css`
    const pathFolderFilesSASS = `${this.basePath}/${appName}/react/**/*.scss`
    const manifestData = await getManifest(pathManifest)

    const pathFilesInSASSToCompileToCSS = [
      "styles/sass/utils/_mixin.scss",
      "styles/sass/utils/_vars.scss",
      pathFolderFilesSASS
    ]

    await new Promise(resolve => {
      gulp.src(pathFilesInSASSToCompileToCSS)
        .pipe(concat(manifestData.file))
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest(pathDestinyCSSCompiled))
        .pipe(gulpDebug(this.debuggerLogs, 'css', 'taskWatch', 'app'))
        .on('end', resolve)
    })

  }

  public compileWatchSASSInStoreTheme = async (gulp: Gulp, sass: any, file: string): Promise<void> => {
    const paths = file.split(/[\\\/]/).filter(item => item !== '..')
    logWatchTask(`[SCSS FILE CHANGED]`, paths.join('/'), 'storeTheme')

    const fileName = paths[paths.length - 1]

    const pathDestinyCSSCompiled = `${this.basePath}/store-theme/styles/css`
    const pathFolderFilesSASS = `${this.basePath}/store-theme/styles/sass/**/${fileName}`

    const pathFilesInSASSToCompileToCSS = [
      "styles/sass/utils/_mixin.scss",
      "styles/sass/utils/_vars.scss",
      pathFolderFilesSASS
    ]

    await new Promise(resolve => {
      gulp.src(pathFilesInSASSToCompileToCSS)
        .pipe(concat(fileName))
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest(pathDestinyCSSCompiled))
        .pipe(gulpDebug(this.debuggerLogs, 'css', 'taskWatch', 'storeTheme'))
        .on('end', resolve)
    })

  }
}
