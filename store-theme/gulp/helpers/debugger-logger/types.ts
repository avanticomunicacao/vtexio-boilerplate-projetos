export type TLogMessageChange = 'storeTheme' | 'app'

export type TLogMessageOption = {
  messageVar?: string
  currentChange?: TLogMessageChange
}

export type TLogMessageType = 'warning' | 'error' | 'info' | 'debug' | 'taskInitial' | 'taskWatch' | 'taskStatistic'
