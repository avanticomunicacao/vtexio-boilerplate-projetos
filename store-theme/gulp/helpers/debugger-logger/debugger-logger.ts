import { hex } from "chalk";
import { TLogMessageChange, TLogMessageType, TLogMessageOption } from "./types";

const hexColors = {
  yellow0: "##ffff66",
  white0: '#ffffff',
  red0: '#ff0000',
  green0: '#00ff55',
  blue0: '#668cff',
  blue1: '#2472C8',

  black0: '#000000',
  orange0: '#ff6600',
  purple0: '#cc00ff',
  pink0: '#ff00cc',
  gray0: '#999999',
  brown0: '#996633',
}

export type TColorsHexNames = keyof typeof hexColors

export class DebuggerLogger {
  private getTimesTamp(date: Date) {
    const formatter = new Intl.DateTimeFormat('pt-BR', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      timeZone: 'UTC'
    });

    const formattedDateTime = formatter.format(date).replace(' ', ' ').replace(/\//g, '-');

    return this.buildMessage(formattedDateTime, 'white0', 'blue0');
  }

  private getStatusType(messageType: TLogMessageType) {
    const status: Record<TLogMessageType, string> = {
      warning: this.buildMessage(this.centerLogMessage('WARNING'), 'blue1'),
      error: this.buildMessage(this.centerLogMessage('ERROR'), 'blue1'),
      info: this.buildMessage(this.centerLogMessage('INFO'), 'blue1'),
      debug: this.buildMessage(this.centerLogMessage('DEBUG'), 'blue1'),
      taskInitial: this.buildMessage(this.centerLogMessage('TASK INITIAL'), 'blue1'),
      taskWatch: this.buildMessage(this.centerLogMessage('TASK WATCH'), 'blue1'),
      taskStatistic: this.buildMessage(this.centerLogMessage('TASK STATISTIC'), 'blue1')

    }

    return status[messageType]
  }

  private getCurrentChangeType(messageType: TLogMessageChange) {
    const status: Record<TLogMessageChange, string> = {
      app: this.buildMessage(this.centerLogMessage('APP'), 'blue1'),
      storeTheme: this.buildMessage(this.centerLogMessage('STORE THEME'), 'blue1')
    }

    return status[messageType]
  }


  public centerLogMessage(message: string, space = 20) {
    const spaceCount = space;
    const spaces = " ".repeat(spaceCount);
    const padding = spaces.slice(0, Math.max(0, Math.floor((spaceCount - message.length) / 2)));

    const extraSpace = (message.length % 2 === 1 ?
      " " :
      "");

    return `${extraSpace}${padding}${message}${padding}`
  }

  private getMessage = (type: TLogMessageType, mainMessage: string, options?: TLogMessageOption) => {
    const args: any[] = []

    args.push(mainMessage)

    if (options?.messageVar) {
      args.push(options.messageVar)
    }
    return args.join(' ')
  }

  public buildMessage(message: string,
    hexName: TColorsHexNames,
    hexBG?: TColorsHexNames) {
    let text: any = ''

    const fontColor: any = hexColors[hexName]
    const bgColor: any = hexColors[hexBG ?? 'green0']

    if (!hexBG) {
      text = hex(fontColor)(message)
    }

    if (hexBG) {
      text = hex(fontColor).bgHex(bgColor)(message)
    }

    return text
  }

  public log(type: TLogMessageType, mainMessage: string, option?: TLogMessageOption) {
    const messageText = this.getMessage(type, mainMessage, { messageVar: option?.messageVar })
    const messageTimesTamp = this.getTimesTamp(new Date())
    const messageType = this.getStatusType(type)
    const messageCurrentChange = this.getCurrentChangeType(option?.currentChange ?? 'app')

    if (!option?.currentChange) {
      console.log(`${messageType} ${messageTimesTamp} ${messageText}`)
    }

    if (option?.currentChange) {
      console.log(`${messageType} ${messageTimesTamp} ${messageCurrentChange} ${messageText}`)
    }

  }

  public logSeparator() {
    const separator = '========================================================================================================'

    const separatorColor = this.buildMessage(separator, 'white0', 'blue1')

    console.log(" ")
    console.log(separatorColor)
    console.log(" ")

  }
}
