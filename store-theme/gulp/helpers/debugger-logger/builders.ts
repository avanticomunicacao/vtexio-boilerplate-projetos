import { DebuggerLogger } from "./debugger-logger";
import { TLogMessageChange } from "./types";

export const logInitialTask = (mainMessage: string, varMessage: string, currentChange?: TLogMessageChange) => {
  const logger = new DebuggerLogger;

  const messageMain = logger.buildMessage(mainMessage, 'blue1');
  const messageVar = logger.buildMessage(varMessage, 'blue1');

  logger.log('taskInitial', messageMain, {
    messageVar,
    currentChange
  })
}

export const logWatchTask = (mainMessage: string, varMessage: string, currentChange?: TLogMessageChange) => {
  const logger = new DebuggerLogger;

  const messageMain = logger.buildMessage(mainMessage, 'blue1');
  const messageVar = logger.buildMessage(varMessage, 'blue1');

  logger.log('taskWatch', messageMain, {
    messageVar,
    currentChange
  })
}

export const logStatistic = (mainMessage: string, varMessage: string, currentChange?: TLogMessageChange, space = 20) => {
  const logger = new DebuggerLogger;

  const messageMain = logger.buildMessage(logger.centerLogMessage(mainMessage, space), 'blue1');
  const messageVar = logger.buildMessage(logger.centerLogMessage(varMessage), 'blue1');

  logger.log('taskStatistic', messageMain, {
    messageVar,
    currentChange
  })
}
