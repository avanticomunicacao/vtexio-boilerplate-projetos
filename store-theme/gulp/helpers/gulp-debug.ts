// @ts-ignore
import GulpDebug from "gulp-debug";
import {DebuggerLogger} from "./debugger-logger/debugger-logger";
import {TLogMessageChange} from "./debugger-logger/types";

type LoggerFunctionNames = 'debug' | 'info' | 'warning' | 'error' | 'sponsor'

export const gulpDebug = (debuggerLogger: DebuggerLogger, typeFile: 'scss' | 'css' | 'normal', typeTask: 'taskInitial' | 'taskWatch', typeChange?: TLogMessageChange) => {
  return GulpDebug({
    minimal: true,
    showCount: false,
    title: "",
    logger: (message: string) => {

      // create test regex
      const isUtilsFolder = /styles.*sass.*utils/.test(message)

      if (isUtilsFolder) {
        return
      }
      const messages = {
        scss: debuggerLogger.buildMessage('[SCSS FILE COMPILED]', 'blue1'),
        css: debuggerLogger.buildMessage('[CSS FILE GENERATED]', 'blue1'),
        normal: debuggerLogger.buildMessage('[INITIAL FILE COMPILED]', 'blue1')
      }

      // logger?.debug(`${messages[typeFile]} ${message}`);

      debuggerLogger.log(typeTask ?? 'taskInitial', messages[typeFile], { messageVar: message, currentChange: typeChange })
    }
  } as any)
}
