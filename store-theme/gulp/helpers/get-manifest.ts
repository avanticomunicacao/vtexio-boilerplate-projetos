import glob from "glob";
import fs from "fs";

export type TManifestData = {
	app: string;
	vendor: string;
	file: string;
	name: string;
}

export const getManifest = async (path: string): Promise<TManifestData> => {
	return new Promise((resolve, reject) => {
		try {
			glob(path, function (er, files) {
				const fileManifest = JSON.parse(fs.readFileSync(files[0], 'utf8'));
				resolve({
					app: `${fileManifest.name}`,
					vendor: `${fileManifest.vendor}`,
					file: `${fileManifest.vendor}.${fileManifest.name}.css`,
					name: `${fileManifest.vendor}.${fileManifest.name}`
				});
			});
		} catch (error) {
			reject(`Erro ao ler o arquivo de manifesto no caminho ${path}: ${error.message}`)
		}
	});
};