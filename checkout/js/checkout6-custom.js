var waitUntilExists = function waitUntilExists(selector, cb) {
  var findElement = setInterval(function () {
    var $el = $(selector);
    if ($el.length) {
      clearInterval(findElement);

      cb($el);
    }
  }, 300);
};

function updateSteps() {
  let steps = ['cart', 'email', 'profile', 'shipping', 'payment'];
  let hash = window?.location?.hash.split('/');

  waitUntilExists('.cart-container', ($step) => {
    let shouldBeActive = hash.includes(steps[0]);
    let shouldBeSuccessful = steps.indexOf(hash[1]) > 0;

    $step.toggleClass('cart-container--active', shouldBeActive);
    $step.toggleClass('cart-container--success', shouldBeSuccessful);
  });

  waitUntilExists('.profile-container', ($step) => {
    let shouldBeActive = hash.includes(steps[1]) || hash.includes(steps[2]);
    let shouldBeSuccessful = steps.indexOf(hash[1]) > 2;

    $step.toggleClass('profile-container--active', shouldBeActive);
    $step.toggleClass('profile-container--success', shouldBeSuccessful);
  });

  waitUntilExists('.shipping-container', ($step) => {
    let shouldBeActive = hash.includes(steps[3]);
    let shouldBeSuccessful = steps.indexOf(hash[1]) > 3;

    $step.toggleClass('shipping-container--active', shouldBeActive);
    $step.toggleClass('shipping-container--success', shouldBeSuccessful);
  });

  waitUntilExists('.payment-container', ($step) => {
    let shouldBeActive = hash.includes(steps[4]);
    let shouldBeSuccessful = steps.indexOf(hash[1]) > 4;

    $step.toggleClass('payment-container--active', shouldBeActive);
    $step.toggleClass('payment-container--success', shouldBeSuccessful);
  });

  waitUntilExists('.payment-data', ($step) => {
    let shouldBeActive = hash.includes(steps[3]);

    $step.toggleClass('shipping--active', shouldBeActive);
  });

  waitUntilExists('#shipping-data', ($step) => {
    let shouldBeActive = hash.includes(steps[4]);

    $step.toggleClass('payment--active', shouldBeActive);
  });
}

function findAttachment(orderForm) {
  const existItem = setInterval(() => {
    if ($('.product-item').length > 0) {
      clearInterval(existItem);
      $('.product-item').each(function (i, item) {
        const skuId = $(item).attr('data-sku');
        orderForm.items.forEach((itemOrder) => {
          if (itemOrder.id === skuId) {
            if (itemOrder.attachmentOfferings.length > 0) {
              itemOrder.attachmentOfferings.forEach((attachmentOffering) => {
                $(item).addClass('haveAttachment');
              });
            }
          }
        });
      });
    }
  }, 500);
}

$(window).on('hashchange', () => {
  updateSteps();
});

$(document).ready(function () {
  updateSteps();
  vtexjs.checkout.getOrderForm().done(function (orderForm) {
    findAttachment(orderForm);
  });
});

$(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
  findAttachment(orderForm);
});
